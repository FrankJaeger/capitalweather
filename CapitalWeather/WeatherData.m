//
//  WeatherData.m
//  CapitalWeather
//
//  Created by dor on 17.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import "WeatherData.h"
#import <AFNetworking/AFNetworking.h>
#import "UIImageView+AFNetworking.h"

@interface WeatherData()

@property (strong, nonatomic) AFHTTPRequestOperationManager *manager;
@property (strong, nonatomic) NSArray *capitals;
@property (strong, nonatomic) NSMutableArray *capitalsInfos;
@property (assign, nonatomic) NSInteger startingPoint;

@property (nonatomic, copy) void (^connectionBlock)(NSString *capitalName);
@property (nonatomic, copy) void (^backgroundDownload)(NSString *capitalName);

@end

@implementation WeatherData

- (id)init {
    self = [ super init ];
    
    if (self) {
        _startingPoint = 0;

        __weak typeof (self) weakSelf = self;
        
        self.connectionBlock = ^(NSString *capitalName) {
            __strong typeof (weakSelf) strongSelf = weakSelf;
            
            NSString *APIUrl = [ NSString stringWithFormat:@"%@%@%@", @"http://api.openweathermap.org/data/2.5/weather?q=", [ capitalName stringByReplacingOccurrencesOfString:@" " withString:@"%20" ], @"&units=metric" ];
            
            NSURL *capUrl = [ NSURL URLWithString:APIUrl ];
            
            NSURLRequest *request = [ NSURLRequest requestWithURL:capUrl ];
            
            AFHTTPRequestOperation *op = [ [AFHTTPRequestOperation alloc ] initWithRequest:request ];
            
            op.responseSerializer = [ AFJSONResponseSerializer serializer ];
            
            if ( [ strongSelf.delegate respondsToSelector:@selector(didFinishedLoadingCapitalWeatherData:forKey:) ] ) {
                [ strongSelf.delegate didFinishedLoadingCapitalWeatherData:@(0) forKey:capitalName ];
            }

            [ op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                if ( [[ responseObject valueForKey:@"cod" ] integerValue ] == 200 ) {
                    
                    NSMutableDictionary *wdataDict = [ NSMutableDictionary dictionaryWithDictionary:                                                @{
                            @"temp" : [ [ responseObject valueForKey:@"main" ] valueForKey:@"temp" ],
                            @"name" : capitalName,
                            @"press": [ [ responseObject valueForKey:@"main" ] valueForKey:@"pressure" ],
                            @"bg"   : [ UIImage new ]
                        }];
  
                    if ( [ strongSelf.delegate respondsToSelector:@selector(didFinishedLoadingCapitalWeatherData:forKey:) ] ) {
                        [ strongSelf.delegate didFinishedLoadingCapitalWeatherData:wdataDict forKey:capitalName ];
                    }
                    
                } else {
                    NSLog(@"Error at: %@", capitalName);
                    if ( [ strongSelf.delegate respondsToSelector:@selector(didFinishedLoadingCapitalWeatherData:forKey:) ] ) {
                        [ strongSelf.delegate didFinishedLoadingCapitalWeatherData:@(-1) forKey:capitalName ];
                    }
                }
                
                strongSelf.backgroundDownload(capitalName);
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                NSLog(@"An error occurred: %@", error.description );
                if ( [ strongSelf.delegate respondsToSelector:@selector(didFinishedLoadingCapitalWeatherData:forKey:) ] ) {
                    [ strongSelf.delegate didFinishedLoadingCapitalWeatherData:@(-1) forKey:capitalName ];
                }
            }];
            
            [ [ NSOperationQueue mainQueue ] addOperation:op ];
        };
        
        self.backgroundDownload = ^(NSString *capitalName) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            
            NSString *GAPIUrlString = [ @"https://ajax.googleapis.com/ajax/services/search/images?v=1.0&imgc=color&imgsz=large&imgtype=photo&q=" stringByAppendingString:[ capitalName stringByReplacingOccurrencesOfString:@" " withString:@"%20" ] ];
            
            NSURL *GAPIUrl = [ NSURL URLWithString:GAPIUrlString ];
            
            NSURLRequest *request = [ NSURLRequest requestWithURL:GAPIUrl ];
            
            AFHTTPRequestOperation *op = [ [AFHTTPRequestOperation alloc] initWithRequest:request ];
            
            op.responseSerializer = [ AFJSONResponseSerializer serializer ];
            
            [ op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                if ( ([responseObject objectForKey:@"responseData"] != nil) && ([ [responseObject objectForKey:@"responseData"] objectForKey:@"results" ] != nil) ) {
                    
                    NSString *imageURL = [[[[ responseObject objectForKey:@"responseData" ] objectForKey:@"results" ] objectAtIndex:0 ] objectForKey:@"url"];
                
                    NSURLRequest *request = [ NSURLRequest requestWithURL:[NSURL URLWithString:imageURL] ];
                
                    AFHTTPRequestOperation *bgop = [ [ AFHTTPRequestOperation alloc ] initWithRequest:request ];
                
                    [ bgop setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                        if( [ strongSelf.delegate respondsToSelector:@selector(didFinishedLoadingBackgroundImage:forKey:) ] ) {
                            [ strongSelf.delegate didFinishedLoadingBackgroundImage:[UIImage imageWithData:responseObject] forKey:capitalName ];
                        }
                    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                        if( [ strongSelf.delegate respondsToSelector:@selector(didFinishedLoadingBackgroundImage:forKey:) ] ) {
                            [ strongSelf.delegate didFinishedLoadingBackgroundImage:@(-1) forKey:capitalName ];
                        }
                    }];
                
                    [ [ NSOperationQueue mainQueue ] addOperation:bgop ];
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"Error in background download at: %@", capitalName);
                
                if( [ strongSelf.delegate respondsToSelector:@selector(didFinishedLoadingBackgroundImage:forKey:) ] ) {
                    [ strongSelf.delegate didFinishedLoadingBackgroundImage:@(-1) forKey:capitalName ];
                }
            }];
            
            [ [ NSOperationQueue mainQueue ] addOperation:op ];
        };
        
    }
    
    return self;
}

- (void)getCapitalsData {
    
    NSString *capitalsString = @"Tirana, Andorra, Yerevan, Vienna, Baku, Minsk, Brussel, Sarajevo, Sofia, Zagreb, Nicosia, Prague, Copenhagen, Tallinn, Helsinki, Paris, Tbilisi, Berlin, Athens, Budapest, Reykjavik, Dublin, Rome, Astana, Pristina, Riga, Vaduz, Vilnius, Luxembourg, Skopje, Valletta, Chisinau, Monaco, Podgorica, Amsterdam, Oslo, Warsaw, Lisbon, Bucharest, Moscow, San Marino, Belgrade, Bratislava, Ljubljana, Madrid, Stockholm, Bern, Ankara, Kiev, London, Vatican City";
    
    _capitals = [ capitalsString componentsSeparatedByString:@", " ];
    
    
    for ( __block NSInteger i = self.startingPoint ; ( (i < self.startingPoint + 5) && (i < self.capitals.count) ) ; i++ ) {
        self.connectionBlock([ self.capitals objectAtIndex:i ]);
    }
    
    self.startingPoint += 5;
}




- (void)getCapitalData:(NSString *)capitalName {
    self.connectionBlock(capitalName);
}
    
- (void)refreshBackground:(NSString *)capitalName {
    self.backgroundDownload(capitalName);
}

@end
