//
//  CityCollectionViewCell.h
//  CapitalWeather
//
//  Created by Przemek on 19.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CORNER_RAD 10

@interface CityCollectionViewCell : UICollectionViewCell

- (void)loadWaitLayout;
- (void)loadDataLayout:(NSDictionary *)data;
- (void)loadErrorLayout;

@end
