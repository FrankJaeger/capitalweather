//
//  CapitalsCollectionViewController.h
//  CapitalWeather
//
//  Created by dor on 17.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeatherData.h"

@interface CapitalsCollectionViewController : UICollectionViewController<WeatherDataDelegate, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@end
