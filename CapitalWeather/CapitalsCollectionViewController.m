//
//  CapitalsCollectionViewController.m
//  CapitalWeather
//
//  Created by dor on 17.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import "CapitalsCollectionViewController.h"
#import "CityCollectionViewCell.h"


@interface CapitalsCollectionViewController ()

@property (strong, nonatomic) WeatherData *data;
@property (strong, nonatomic) NSMutableDictionary *cvDataSource;
@property (strong, nonatomic) NSMutableArray *keysForDataSource;
@property (strong, nonatomic) UIButton *moreButton;

@end

@implementation CapitalsCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.collectionView registerClass:[CityCollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"Footer"];
    
    self.data = [ WeatherData new ];
    self.data.delegate = self;
    
    self.cvDataSource = [ NSMutableDictionary new ];
    self.keysForDataSource = [ NSMutableArray new ];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [ self.data getCapitalsData ];
    });
    
    self.moreButton = [ UIButton new ];
    
    [ self.moreButton setTitle:@"More..." forState:UIControlStateNormal ];
    [ self.moreButton.layer setCornerRadius:10 ];
    [ self.moreButton.layer setBorderColor:[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1].CGColor ];
    [ self.moreButton.layer setBorderWidth:2.0f ];
    [ self.moreButton setTitleColor:[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1] forState:UIControlStateNormal ];
    [ self.moreButton setAlpha:0.55 ];
    [ self.moreButton setBackgroundColor:[UIColor blackColor] ];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.cvDataSource.count;
}

- (CityCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CityCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    if ( [ [ _cvDataSource objectForKey:[ _keysForDataSource objectAtIndex:indexPath.row ] ] isKindOfClass:[ NSDictionary class ] ] ) {
        [ cell loadDataLayout:[ self.cvDataSource objectForKey:[ self.keysForDataSource objectAtIndex:indexPath.row ] ] ];
        
    } else if ( [ [ _cvDataSource objectForKey:[ _keysForDataSource objectAtIndex:indexPath.row ] ] isEqualToNumber:@(0) ] ) {
        [ cell loadWaitLayout ];
    } else {
        [ cell loadErrorLayout ];
        
    }

    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *reusableView = [ self.collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"Footer" forIndexPath:indexPath ];
    
    [ self.moreButton setFrame:CGRectMake(reusableView.frame.size.width*0.2, reusableView.frame.size.height*0.1, reusableView.frame.size.width*0.6, reusableView.frame.size.height*0.8) ];
    
    [ reusableView addSubview:self.moreButton ];
    
    return reusableView;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplaySupplementaryView:(UICollectionReusableView *)view forElementKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath {
    UITapGestureRecognizer *moreTapGRec = [ [ UITapGestureRecognizer alloc ] initWithTarget:self action:@selector(getMoreClicked) ];
    
    [ self.moreButton addGestureRecognizer:moreTapGRec ];
}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/


// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ( ([ [ self.cvDataSource objectForKey:[self.keysForDataSource objectAtIndex:indexPath.row] ] isKindOfClass:[NSDictionary class] ] && ![[[ self.cvDataSource objectForKey:[self.keysForDataSource objectAtIndex:indexPath.row] ] objectForKey:@"bg"] isEqual:@(-1)]) || [ [ self.cvDataSource objectForKey:[self.keysForDataSource objectAtIndex:indexPath.row] ] isEqual:@(0) ] ) return NO;
   
    return YES;
}


/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ( !([[self.cvDataSource objectForKey:[self.keysForDataSource objectAtIndex:indexPath.row]] isEqual:@(-1)] || [[self.cvDataSource objectForKey:[self.keysForDataSource objectAtIndex:indexPath.row]] isEqual:@(0)] ) && [[[self.cvDataSource objectForKey:[self.keysForDataSource objectAtIndex:indexPath.row]] objectForKey:@"bg"] isEqual:@(-1)] ) {
        [ self.data refreshBackground:[ self.keysForDataSource objectAtIndex:indexPath.row ] ];
    } else {
        [ self.data getCapitalData:[ self.keysForDataSource objectAtIndex:indexPath.row ] ];
    }
}

#pragma mark Weather Data Delegate

-(void)didFinishedLoadingCapitalWeatherData:(id)capital forKey:(NSString *)key {
    [ self.cvDataSource setObject:capital forKey:key ];
    
    if ( ![self.keysForDataSource containsObject:key] ) {
        [ self.keysForDataSource addObject:key ];
    }
    
    [ self.collectionView reloadData ];

}

- (void)didFinishedLoadingBackgroundImage:(id)image forKey:(NSString *)key {
    [ [ self.cvDataSource objectForKey:key ] setObject:image forKey:@"bg" ];
    [ self.collectionView reloadData ];
}

#pragma mark UI Elements Events

- (void)getMoreClicked {
   dispatch_async(dispatch_get_main_queue(), ^{
       [ self.data getCapitalsData ];
   });
}

#pragma mark Cells Layout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake([UIScreen mainScreen].bounds.size.width/3.7, [UIScreen mainScreen].bounds.size.width/3.7);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(15, 15, 15, 15);
}

@end




