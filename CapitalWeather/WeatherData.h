//
//  WeatherData.h
//  CapitalWeather
//
//  Created by dor on 17.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol WeatherDataDelegate <NSObject>

- (void)didFinishedLoadingCapitalWeatherData:(id)capital forKey:(NSString *)key;
- (void)didFinishedLoadingBackgroundImage:(id)image forKey:(NSString *)key;

@end

@interface WeatherData : NSObject

@property (weak, nonatomic) id<WeatherDataDelegate> delegate;

- (void)getCapitalsData;
- (void)getCapitalData:(NSString *)capitalName;
- (void)refreshBackground:(NSString *)capitalName;

@end
