//
//  CityCollectionViewCell.m
//  CapitalWeather
//
//  Created by Przemek on 19.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import "CityCollectionViewCell.h"

@implementation CityCollectionViewCell

- (id)initWithFrame:(CGRect)frame {
    self = [ super initWithFrame:frame ];

    if (self) {
        self.layer.cornerRadius = CORNER_RAD;
    }
    
    return self;
}

- (void)loadWaitLayout {
    [ self initLayout:[UIColor colorWithRed:0.88 green:0.88 blue:0.88 alpha:1.0] withAlpha:0.5 ];
    
    UIActivityIndicatorView *indicator = [ UIActivityIndicatorView new ];
    
    indicator.center = self.center;
    indicator.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    indicator.transform = CGAffineTransformMakeScale(1.5, 1.5);
    
    [ self addSubview:indicator ];
    [ indicator startAnimating ];
    
}

- (void)loadDataLayout:(NSDictionary *)data {
    
    CGFloat temperature = round([[ data objectForKey:@"temp" ] floatValue]);
    UIColor *tempColor;
    
    if ( temperature < 0 ) {
        if ( temperature >= -20 ) {
            tempColor = [ UIColor colorWithRed:-(temperature/20) green:0.8 blue:1 alpha:1 ];
        } else {
            tempColor = [ UIColor whiteColor ];
        }
    } else if ( temperature >= 0 ) {
        if ( temperature < 40 ) {
            tempColor = [ UIColor colorWithRed:1 green:( 1 - temperature/40 ) blue:0 alpha:1 ];
        } else {
            tempColor = [ UIColor redColor ];
        }
    }
    
    [ self initLayout:tempColor withAlpha:1 ];
    
    UIImageView *backgroundImage = [ [UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height) ];
    UIView *backgroundOverlay = [ [ UIView alloc ] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height) ];
    UILabel *capitalName = [ [ UILabel alloc ] initWithFrame:CGRectMake(self.frame.size.width*0.1, (self.frame.size.height/4)*3, self.frame.size.width*0.8, self.frame.size.height/4) ];
    UILabel *temperatureValue = [ [ UILabel alloc ] initWithFrame:CGRectMake(0, self.frame.size.height/4, self.frame.size.width, self.frame.size.height/2) ];
    UILabel *pressureValue = [ [ UILabel alloc ] initWithFrame:CGRectMake(self.frame.size.width*0.1, 0, self.frame.size.width*0.8, self.frame.size.height/5) ];
    
    if ( ![ [ data objectForKey:@"bg" ] isEqual:@(-1) ] ) {
        backgroundImage.image = [ data objectForKey:@"bg" ];
        backgroundImage.layer.cornerRadius = CORNER_RAD;
        backgroundImage.layer.masksToBounds = YES;
        backgroundImage.alpha = 0.7;
    } else {
        backgroundImage.image = [ UIImage imageNamed:@"RefreshWhite" ];
        backgroundImage.frame = CGRectMake(self.frame.size.width/20, self.frame.size.height/20, self.frame.size.width/7, self.frame.size.width/7);
        
        CABasicAnimation *refreshPulse = [ CABasicAnimation animationWithKeyPath:@"transform.scale" ];
        
        refreshPulse.duration = 1;
        refreshPulse.repeatCount = HUGE_VALF;
        refreshPulse.autoreverses = YES;
        refreshPulse.fromValue = [ NSNumber numberWithFloat: 1.0 ];
        refreshPulse.toValue = [ NSNumber numberWithFloat: 0.8 ];
        
        [ backgroundImage.layer addAnimation:refreshPulse forKey:nil ];
    }
    
    backgroundOverlay.backgroundColor = [ UIColor colorWithRed:0 green:0 blue:0 alpha:0.4 ];
    backgroundOverlay.layer.cornerRadius = CORNER_RAD;
    
    capitalName.text = [ data objectForKey:@"name" ];
    capitalName.textColor = [ UIColor whiteColor ];
    capitalName.textAlignment = NSTextAlignmentCenter;
    capitalName.font = [ UIFont fontWithName:@"HelveticaNeue-Light"  size:self.frame.size.height/6.5 ];
    capitalName.adjustsFontSizeToFitWidth = YES;
    capitalName.minimumScaleFactor = 0.5;
    
    temperatureValue.text = [NSString stringWithFormat:@"%g", round([[ data objectForKey:@"temp" ] floatValue]) ];
    temperatureValue.textColor = tempColor;
    temperatureValue.textAlignment = NSTextAlignmentCenter;
    temperatureValue.font = [ UIFont fontWithName:@"HelveticaNeue-Light"  size:self.frame.size.height/2 ];
    temperatureValue.adjustsFontSizeToFitWidth = YES;
    temperatureValue.minimumScaleFactor = 0.5;
    
    pressureValue.text = [ NSString stringWithFormat:@"%.2f %@", [ [ data objectForKey:@"press" ] floatValue ], @"hPa" ];
    pressureValue.textColor = [ UIColor whiteColor ];
    pressureValue.textAlignment = NSTextAlignmentCenter;
    pressureValue.font = [ UIFont fontWithName:@"HelveticaNeue-Light"  size:self.frame.size.height/10 ];
    pressureValue.adjustsFontSizeToFitWidth = YES;
    pressureValue.minimumScaleFactor = 0.5;
    
    [ self addSubview:backgroundImage ];
    [ self addSubview:backgroundOverlay ];
    [ self addSubview:capitalName ];
    [ self addSubview:temperatureValue ];
    [ self addSubview:pressureValue ];
}

- (void)loadErrorLayout {
    [ self initLayout:[UIColor colorWithRed:0.69 green:0.00 blue:0.00 alpha:1.0] withAlpha:0.8 ];
    
    UILabel *errorLabel = [ [ UILabel alloc ] initWithFrame:CGRectMake(0, (self.frame.size.height/3)*2, self.frame.size.width, self.frame.size.height/3) ];
    UIImageView *refreshImage = [ [ UIImageView alloc ] initWithImage:[ UIImage imageNamed:@"Refresh" ] ];
    
    errorLabel.text = @"Error!";
    errorLabel.textColor = [UIColor colorWithRed:0.69 green:0.00 blue:0.00 alpha:1.0];
    errorLabel.font = [ UIFont fontWithName:@"HelveticaNeue-Light"  size:self.frame.size.height/4.2 ];
    errorLabel.textAlignment = NSTextAlignmentCenter;
    errorLabel.adjustsFontSizeToFitWidth = YES;
    errorLabel.minimumScaleFactor = 0.5;
    
    refreshImage.frame = CGRectMake(self.frame.size.width/4, self.frame.size.height/5, self.frame.size.width/2, self.frame.size.height/2);
    
    CABasicAnimation *refreshPulse = [ CABasicAnimation animationWithKeyPath:@"transform.scale" ];
    CABasicAnimation *refreshCellPulse = [ CABasicAnimation animationWithKeyPath:@"transform.scale" ];
    
    refreshPulse.duration = 1;
    refreshPulse.repeatCount = HUGE_VALF;
    refreshPulse.autoreverses = YES;
    refreshPulse.fromValue = [ NSNumber numberWithFloat: 1.0 ];
    refreshPulse.toValue = [ NSNumber numberWithFloat: 0.8 ];
    
    refreshCellPulse.duration = 0.5;
    refreshCellPulse.repeatCount = HUGE_VALF;
    refreshCellPulse.autoreverses = YES;
    refreshCellPulse.fromValue = [ NSNumber numberWithFloat:1.0 ];
    refreshCellPulse.toValue = [ NSNumber numberWithFloat:0.95 ];
    
    [ refreshImage.layer addAnimation:refreshPulse forKey:nil ];
    [ self.layer addAnimation:refreshCellPulse forKey:nil ];
    
    [ self addSubview:errorLabel ];
    [ self addSubview:refreshImage ];
}


- (void)initLayout:(UIColor *)borderColor withAlpha:(CGFloat)alpha {
    [ [ self subviews ] makeObjectsPerformSelector:@selector(removeFromSuperview) ];

    self.layer.borderColor = borderColor.CGColor;
    self.layer.borderWidth = 2.0f;
    
    self.alpha = alpha;
}

@end
